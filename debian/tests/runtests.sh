#!/bin/sh

set -e

rm -f /tmp/pwn

wsgidav --port=8080 --host=127.0.0.1 --root=/tmp --auth=anonymous &
until nc -z -w 1 127.0.0.1 8080; do sleep 1; done

davtest -url http://127.0.0.1:8080 | grep 'PUT.*SUCCEED:'
davtest -url http://127.0.0.1:8080 -uploadfile=/etc/passwd -uploadloc=pwn

pkill wsgidav

test -f /tmp/pwn
